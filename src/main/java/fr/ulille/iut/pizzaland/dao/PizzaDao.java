package fr.ulille.iut.pizzaland.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.customizer.BindBeanList;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {
	
    @SqlUpdate("CREATE TABLE IF NOT EXISTS Pizza (id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE NOT NULL);"
    		+  "CREATE TABLE IF NOT EXISTS IngredientsInPizza(idP VARCHAR(128) PRIMARY KEY, idI VARCHAR(128) PRIMARY KEY);")
    void createTable();
    
    @SqlUpdate("DROP TABLE IF EXISTS Pizza;"
    		+  "DROP TABLE  IF EXIST IngredientInPizza;")
    void dropTable();
    
    @SqlUpdate("INSERT INTO Pizza (id, name) VALUES (:id, :name);"
    		+  "INSERT INTO IngredientInPizza(idP,idI) VALUES(:id, :idI")
    void insert(@BindBean Pizza pizza, @BindBeanList(value = "idI",propertyNames = { "id" }) ArrayList<Ingredient> ingredients);
    
    @SqlUpdate("DELETE FROM IngrdientInPizza WHERE id = :id"
    		+  "DELETE FROM Pizza WHERE id = :id")
    void remove(@Bind("id") UUID id);
    
    @SqlQuery("SELECT * FROM Pizza WHERE name = :name")
    @RegisterBeanMapper(Pizza.class)
    Pizza findByName(@Bind("name") String name);

    @SqlQuery("SELECT * FROM Pizza")
    @RegisterBeanMapper(Pizza.class)
    List<Pizza> getAll();

    @SqlQuery("SELECT * FROM Pizza WHERE id = :id")
    @RegisterBeanMapper(Pizza.class)
    Pizza findById(@Bind("id") UUID id);
}