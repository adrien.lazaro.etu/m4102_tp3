package fr.ulille.iut.pizzaland.dto;

import java.util.ArrayList;
import java.util.UUID;

import fr.ulille.iut.pizzaland.beans.Ingredient;

public class PizzaDto {
	private UUID id;
	private String name;
	private ArrayList<Ingredient> ingredient;
	
	public PizzaDto() {}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<Ingredient> getIngredient() {
		return ingredient;
	}
	public void setIngredient(ArrayList<Ingredient> ingredient) {
		this.ingredient = ingredient;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
}
