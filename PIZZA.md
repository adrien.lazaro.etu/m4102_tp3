| URI                      | Opération   | MIME                                                         | Requête         | Réponse                                                              |
| :----------------------- | :---------- | :---------------------------------------------               | :--             | :----------------------------------------------------                |
| /Pizza				   | GET		 | <-application/json<br><-application/xml 						|				  | Liste des pizzas													
| /Pizza/{id}			   | GET		 | <-application/json<br><-application/xml						|				  | Une pizza en particulier ou 404
| /Pizza    			   | POST		 | <-application/json<br><-application/xml						| Pizza			  | Creer une pizza ou 409 si existe deja
| /Pizza/{id}			   | DELETE		 |																|				  | Supprime une pizza